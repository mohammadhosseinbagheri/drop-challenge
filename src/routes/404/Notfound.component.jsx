import React from "react";

const Notfound = () => {
  return (
    <div
      style={{
        justifyContent: "center",
        alignItems: "center",
        display: "flex",
        margin: "auto 10px",
        marginTop: 80,
      }}
    >
      404 Notfound
    </div>
  );
};

export default Notfound;
