import React from "react";
import CheckoutComponent from "../../components/checkout/checkout.component";

const CheckoutRoute = () => {
  return (
    <div>
      <CheckoutComponent />
    </div>
  );
};

export default CheckoutRoute;
