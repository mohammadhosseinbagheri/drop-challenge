import React, { useEffect } from "react";
import Header from "./components/header/header.component";
import TabBar from "./components/tabbar/tabbar.component";
import { Redirect, Route, Switch } from "react-router-dom";
import CheckoutRoute from "./routes/checkout-container-route/checkout-container-route.component";
import { connect } from "react-redux";
import Notfound from "./routes/404/Notfound.component";
import { purgeCartItem } from "./redux/add-to-cart/action/add-to-cart.action";
import Favorite from "./routes/favorite/Favorite.components";
import { purgeFavorite } from "./redux/collections/action/collection.acation";

const App = (props) => {
  const { addedItem, purge, purgeFavorite } = props;
  useEffect(() => {
    isExpire();
    isExpireFavorite();
  }, []);

  const isExpire = () => {
    const expireTime = JSON.parse(localStorage.getItem("expireTime"));
    const now = new Date().getTime();
    if (expireTime < now) {
      purge();
      return;
    }
  };
  const isExpireFavorite = () => {
    const expireTime = JSON.parse(localStorage.getItem("favoriteExpire"));
    const now = new Date().getTime();
    if (expireTime < now) {
      purgeFavorite();
      return;
    }
  };
  return (
    <div>
      <Header />
      <Switch>
        <Route exact path="/" component={TabBar} />
        <Route
          exact
          path="/checkout"
          render={(props) =>
            addedItem.length === 0 ? (
              <Redirect to="/" />
            ) : (
              <CheckoutRoute {...props} />
            )
          }
        />
        <Route exact path="/favorite" component={Favorite} />
        <Route exact path="" component={Notfound} />
      </Switch>
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    addedItem: state.addedToCart,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    purge: () => dispatch(purgeCartItem()),
    purgeFavorite: () => dispatch(purgeFavorite()),
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(App);
