export const addToFavorite = (state, addToFavorite) => {
  const exist = state.find((item) => item.id === addToFavorite.id);
  if (exist) {
    return state.filter((item) => item.id !== addToFavorite.id);
  }
  return [...state, addToFavorite];
};
