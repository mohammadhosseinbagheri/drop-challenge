import { takeLatest, call, put, all } from "redux-saga/effects";
import { FETCH_COLLECTION_START } from "../types/collection.types";
import {
  fetchCollectionFailure,
  fetchCollectionSuccess,
} from "./collection.acation";
import axios from "axios";


export function* fetchCollectionStartAsync() {
  try {
    const response = yield axios.get("https://api.punkapi.com/v2/beers");
    const data = yield response.data;
    yield put(fetchCollectionSuccess(data));
  } catch (error) {
    yield put(fetchCollectionFailure(error));
  }
}

export function* fetchCollectionStart() {
  yield takeLatest(FETCH_COLLECTION_START, fetchCollectionStartAsync);
}

export default function* collectionSaga() {
  yield all([call(fetchCollectionStart)]);
}
