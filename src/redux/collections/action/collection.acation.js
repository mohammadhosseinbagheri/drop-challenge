import {
  FETCH_COLLECTION_START,
  FETCH_COLLECTION_FAILURE,
  FETCH_COLLECTION_SUCCESS,
  SELECTED_ITEM,
  PURGE_FAVORITE,
} from "../types/collection.types";

export const fetchCollectionStart = () => {
  return {
    type: FETCH_COLLECTION_START,
  };
};
export const fetchCollectionSuccess = (collection) => {
  return {
    type: FETCH_COLLECTION_SUCCESS,
    payload: collection,
  };
};
export const fetchCollectionFailure = (error) => {
  return {
    type: FETCH_COLLECTION_FAILURE,
    payload: error,
  };
};

export const selectItem = (item) => {
  return {
    type: SELECTED_ITEM,
    payload: item,
  };
};
export const purgeFavorite = () => {
  return {
    type: PURGE_FAVORITE,
  };
};
