import { addToFavorite } from "../collection.utils";
const {
  FETCH_COLLECTION_START,
  FETCH_COLLECTION_SUCCESS,
  FETCH_COLLECTION_FAILURE,
  SELECTED_ITEM,
  PURGE_FAVORITE,
} = require("../types/collection.types");

const INITIALSTATE = {
  collection: [],
  error: null,
  isLoading: true,
  selectedItem: [],
};

const collectionReducer = (state = INITIALSTATE, action) => {
  switch (action.type) {
    case FETCH_COLLECTION_START:
      return {
        ...state,
        isLoading: true,
      };
    case FETCH_COLLECTION_SUCCESS:
      return {
        ...state,
        collection: action.payload,
        isLoading: false,
        error: null,
      };
    case FETCH_COLLECTION_FAILURE:
      return {
        ...state,
        error: action.payload,
      };
    case SELECTED_ITEM:
      return {
        ...state,
        selectedItem: addToFavorite(state.selectedItem, action.payload),
      };
    case PURGE_FAVORITE:
      return {
        ...state,
        selectedItem: [],
      };
    default:
      return state;
  }
};
export default collectionReducer;
