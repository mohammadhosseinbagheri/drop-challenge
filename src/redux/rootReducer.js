import { combineReducers } from "redux";
import addToCartReducer from "./add-to-cart/reducer/add-to-cart.reducer";
import collectionReducer from "./collections/reducer/collection.reducer";
import modalboxReducer from "./modalbox/reducer/modalbox.reducer";
const rootReducer = combineReducers({
  collections: collectionReducer,
  showModal: modalboxReducer,
  addedToCart: addToCartReducer,
});

export default rootReducer;
