import { all, call } from "redux-saga/effects";
import collectionSaga from "./collections/action/collection.saga";

export default function* rootSaga() {
  yield all([call(collectionSaga)]);
}
