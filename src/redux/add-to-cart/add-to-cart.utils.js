export const addToCart = (state, addedItem) => {
  const isExist = state.find((item) => item.id === addedItem.id);
  if (isExist) {
    return state.map((item) => {
      if (item.id === addedItem.id) {
        return { ...item, quantity: item.quantity + 1 };
      }
      return item;
    });
  }
  return [...state, { ...addedItem, quantity: 1 }];
};

export const removeFromCart = (state, removedItem) => {
  const isExist = state.find((item) => item.id === removedItem.id);
  if (isExist.quantity === 1) {
    return state.filter((item) => item.id !== removedItem.id);
  }
  return state.map((item) =>
    item.id === removedItem.id ? { ...item, quantity: item.quantity - 1 } : item
  );
};

export const deleteItemFromCart = (state, deletedItem) => {
  return state.filter((item) => item.id != deletedItem.id);
};
