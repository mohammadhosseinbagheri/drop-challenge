import {
  ADD_TO_CART,
  DELETE_CART,
  REMOVE_FROM_CART,
  DELETE_ITEM_FROM_CART,
  PURGE,
} from "../types/add-to-cart.types";

export const addToCartAction = (item) => {
  return {
    type: ADD_TO_CART,
    payload: item,
  };
};

export const removeFromCartAction = (item) => {
  return {
    type: REMOVE_FROM_CART,
    payload: item,
  };
};

export const deleteItemFromCart = (item) => {
  return {
    type: DELETE_ITEM_FROM_CART,
    payload: item,
  };
};

export const deleteCartAction = () => {
  return {
    type: DELETE_CART,
  };
};

export const purgeCartItem = () => {
  return {
    type: PURGE,
  };
};
