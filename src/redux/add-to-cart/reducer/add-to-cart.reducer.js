import {
  addToCart,
  deleteItemFromCart,
  removeFromCart,
} from "../add-to-cart.utils";
import {
  ADD_TO_CART,
  DELETE_CART,
  DELETE_ITEM_FROM_CART,
  PURGE,
  REMOVE_FROM_CART,
} from "../types/add-to-cart.types";

const INITIALSTATE = [];

const addToCartReducer = (state = INITIALSTATE, action) => {
  switch (action.type) {
    case ADD_TO_CART:
      return addToCart(state, action.payload);
    case REMOVE_FROM_CART:
      return removeFromCart(state, action.payload);
    case DELETE_ITEM_FROM_CART:
      return deleteItemFromCart(state, action.payload);
    case DELETE_CART:
      return [];
    case PURGE:
      return [];
    default:
      return state;
  }
};

export default addToCartReducer;
