import { SHOPPING_CART_MODAL, SHOW_MODALBOX } from "../types/modalbox.types";

export const showModalbox = (item) => {
  return {
    type: SHOW_MODALBOX,
    payload: item,
  };
};

export const shoppingCartModal = () => {
  return {
    type: SHOPPING_CART_MODAL,
  };
};
