const {
  SHOW_MODALBOX,
  SHOPPING_CART_MODAL,
} = require("../types/modalbox.types");

const INITIALSTATE = {
  isShow: false,
  modalItem: [],
  shoppingCartModal: false,
};

const modalboxReducer = (state = INITIALSTATE, action) => {
  switch (action.type) {
    case SHOPPING_CART_MODAL:
      return {
        ...state,
        shoppingCartModal: !state.shoppingCartModal,
      };
    case SHOW_MODALBOX:
      return {
        isShow: !state.isShow,
        modalItem: action.payload,
        shoppingCartModal: state.shoppingCartModal,
      };
    default:
      return state;
  }
};

export default modalboxReducer;
