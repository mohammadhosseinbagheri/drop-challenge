import React from "react";

const CheckoutItem = (props) => {
  const { item, addToCart, removeFromCart } = props;
  return (
    <div className="checkout-item">
      <div className="checkout-img">
        <img src={item.image_url} style={{ width: "40px", height: "100%" }} />
      </div>
      <div className="checkout-name">{item.name}</div>
      <div className="checkout-count">{item.abv}</div>
      <div className="checkout-amaliat">
        <button
          onClick={() => removeFromCart(item)}
          className={"checkout-amal-button"}
        >
          <p>&#60;</p>
        </button>
        <h4>{item.quantity}</h4>
        <button
          onClick={() => addToCart(item)}
          className={"checkout-amal-button"}
        >
          <p>&#x3e;</p>
        </button>
      </div>
    </div>
  );
};

export default CheckoutItem;
