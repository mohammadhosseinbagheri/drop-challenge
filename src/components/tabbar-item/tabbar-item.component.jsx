import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import "./tabbar-item.styles.scss";
import SortItem from "../sort-item/sort-item.component";
import TabbarItemContent from "./tabar-item-content.component";
const TabBarItem = (props) => {
  const { item, isLoading, isShow } = props;
  const [value, setValue] = useState("abvAscending");
  const handleChange = (event) => {
    const { name, value } = event.target;
    if (value === "abvAscending") {
      setValue("abvAscending");
      return;
    }
    if (value === "abvDescending") {
      setValue("abvDescending");
      return;
    }
    if (value === "nameAscending") {
      setValue("nameAscending");
      return;
    }
    if (value === "nameDescending") {
      setValue("nameDescending");
      return;
    }
  };

  return (
    <>
      <div
        className="tabbar-item-catgories"
      >
        <SortItem
          name="abv ascending"
          handleChange={handleChange}
          value={"abvAscending"}
          checked={value == "abvAscending" ? true : false}
        />
        <SortItem
          name="abv descending"
          handleChange={handleChange}
          value={"abvDescending"}
          checked={value == "abvDescending" ? true : false}
        />
        <SortItem
          name="name ascending"
          handleChange={handleChange}
          value={"nameAscending"}
          checked={value == "nameAscending" ? true : false}
        />
        <SortItem
          name="name descending"
          handleChange={handleChange}
          value={"nameDescending"}
          checked={value == "nameDescending" ? true : false}
        />
      </div>
      <TabbarItemContent
        value={value}
        isShow={isShow}
        items={item}
        isLoading={isLoading}
      />
    </>
  );
};

const mapStateToProps = (state) => {
  return {
    items: state.collections.collection,
    isLoading: state.collections.isLoading,
    isShow: state.showModal.isShow,
  };
};
export default connect(mapStateToProps)(TabBarItem);
