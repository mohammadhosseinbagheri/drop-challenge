import React from "react";
import Modalbox from "../modalbox/modalbox.component";
import CardItem from "../card-item/card-item.component";

const TabbarItemContent = (props) => {
  const { items, isShow, isLoading, value } = props;
  const sortView = (item) => {
    if (value === "abvAscending") {
      return item.sort((a, b) => a.abv - b.abv);
    }
    if (value === "abvDescending") {
      return item.sort((a, b) => b.abv - a.abv);
    }
    if (value === "nameAscending") {
      return item.sort((a, b) => {
        let nameA = a.name.toUpperCase();
        let nameB = b.name.toUpperCase();
        if (nameA < nameB) {
          return -1;
        }
        if (nameA > nameB) {
          return 1;
        }
        return 0;
      });
    }
    if (value === "nameDescending") {
      return item.sort((a, b) => {
        let nameA = a.name.toUpperCase();
        let nameB = b.name.toUpperCase();
        if (nameA < nameB) {
          return 1;
        }
        if (nameA > nameB) {
          return -1;
        }
        return 0;
      });
    }
  };

  return (
    <div className="tabbar-item-container">
      {items
        ? sortView(items).map((item, index) => (
            <CardItem
              key={index}
              index={index}
              isLoading={isLoading}
              item={item}
            />
          ))
        : null}
      {isShow ? <Modalbox /> : null}
    </div>
  );
};

export default TabbarItemContent;
