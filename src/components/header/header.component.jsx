import React from "react";
import "./header.styles.scss";
import { Star, ShoppingBasket } from "@material-ui/icons";
import { connect } from "react-redux";
import ShoppingCart from "../shopping-cart/shopping-cart.component";
import { shoppingCartModal } from "../../redux/modalbox/action/modalbox.action";
import { Link } from "react-router-dom";
import FavoriteHeaderComponent from "../favorite-item/favorite-header.component";

const Header = (props) => {
  const { count, shoppingModalShow, shoppingCartModal } = props;
  console.log(shoppingModalShow);
  return (
    <div className="header-container">
      <div className={"header-content"}>
        <div className="header-left-section">
          <button
            className={"basket-button"}
            onClick={() => shoppingCartModal()}
          >
            <h5 className={"count-basket"}>{count}</h5>
            <ShoppingBasket color="inherit" style={{ cursor: "pointer" }} />
          </button>
          <FavoriteHeaderComponent />
        </div>
        <Link to="/" style={{ color: "white", textDecoration: "none" }}>
          DROP CHALLENGE
        </Link>
      </div>
      {shoppingModalShow ? <ShoppingCart /> : null}
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    count: state.addedToCart.reduce(
      (accumulate, item) => accumulate + item.quantity,
      0
    ),
    shoppingModalShow: state.showModal.shoppingCartModal,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    shoppingCartModal: () => dispatch(shoppingCartModal()),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Header);
