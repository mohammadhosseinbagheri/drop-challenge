import React from "react";
import { connect } from "react-redux";
import {
  addToCartAction,
  removeFromCartAction,
} from "../../redux/add-to-cart/action/add-to-cart.action";
import CheckoutItem from "../checkout-item/checkout-item.component";
import "./checkout.styles.scss";
const CheckoutComponent = (props) => {
  const { addedItem, addToCart, removeFromCart, totalPrice } = props;
  console.log(addedItem);
  return (
    <div className="checkout-container">
      <div className="checkout-content-container">
        <div className="checkout-content">
          <div className="checkout-title">
            <div className="checkout-img">Image</div>
            <div className="checkout-name">Title</div>
            <div className="checkout-count">Price</div>
            <div className="checkout-amaliat">Count</div>
          </div>
          {addedItem.map((item, index) => (
            <CheckoutItem removeFromCart={removeFromCart} addToCart={addToCart} item={item} key={index} index={index} />
          ))}
          <div className={"checkout-footer"}>
            <div className="checkout-total-price">
              <h4>Total : ${totalPrice(addedItem).toFixed(2)}</h4>
            </div>
            {addedItem.length !== 0 ? (
              <button className={"payment-button"}>payment</button>
            ) : null}
          </div>
        </div>
      </div>
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    addedItem: state.addedToCart,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    addToCart: (item) => dispatch(addToCartAction(item)),
    removeFromCart: (item) => dispatch(removeFromCartAction(item)),
    totalPrice: (item) =>
      item.reduce(
        (accumulate, element) => accumulate + element.abv * element.quantity,
        0
      ),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(CheckoutComponent);
