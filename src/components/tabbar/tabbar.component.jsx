import React, { useState, useEffect } from "react";
import { Tabs, Tab } from "@material-ui/core";
import { connect } from "react-redux";
import { fetchCollectionStart } from "../../redux/collections/action/collection.acation";

import "./tabbar.styles.scss";
import TabBarItem from "../tabbar-item/tabbar-item.component";
import SoftdrinkWithPizza from "../soft-drink-with-pizza/soft-drink-with-pizza.component";

const TabBar = (props) => {
  const { fetchCollection, item } = props;
  const [value, setValue] = useState(0);

  useEffect(() => {
    const getCollcetion = async () => {
      fetchCollection();
    };
    getCollcetion();
  }, []);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };
  const pizzaData = (item) => {
    const filteredData = [];
    item.map((collectionItem) => {
      collectionItem.food_pairing.map((food_pairing) =>
        food_pairing.includes("pizza")
          ? filteredData.push(collectionItem)
          : null
      );
    });
    return filteredData;
  };
  const steakData = (item) => {
    const filteredData = [];
    item.map((collectionItem) => {
      collectionItem.food_pairing.map((food_pairing) =>
        food_pairing.includes("steak")
          ? filteredData.push(collectionItem)
          : null
      );
    });
    return filteredData;
  };
  const filter = (index, item) => {
    if (index === 1) {
      return pizzaData(item);
    }
    return steakData(item);
  };
  return (
    <div className="tabbar-container">
      <Tabs value={value} onChange={handleChange}>
        <Tab label="All" />
        <Tab label="Soft drinks with Pizza" />
        <Tab label="Soft drinks with Steak" />
      </Tabs>
      {value === 0 ? (
        <TabBarItem item={item} index={0} />
      ) : value === 1 ? (
        <SoftdrinkWithPizza item={filter(1, item)} index={1} />
      ) : (
        <SoftdrinkWithPizza item={filter(2, item)} index={2} />
      )}
    </div>
  );
};

const mapDispatchToProps = (dispatch) => {
  return {
    fetchCollection: () => dispatch(fetchCollectionStart()),
  };
};
const mapStateToProps = (state) => {
  return {
    item: state.collections.collection,
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(TabBar);
