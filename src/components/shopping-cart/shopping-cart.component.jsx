import React from "react";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import ShoppingCartItem from "../shopping-cart-item/shopping-cart-item.component";
import CheckoutButton from "../checkout-button/checkout-botton.component";
import "../shopping-cart/shopping-cart.styles.scss";

const ShoppingCart = (props) => {
  const { item, history } = props;
  const checkoutPage = () => {
    history.push("/checkout");
  };
  return (
    <div className="container">
      <h5 className={"title"}>Your Cart</h5>
      <div className="content">
        {item.length != 0 ? (
          item.map((element) => <ShoppingCartItem key={element.id} element={element} />)
        ) : (
          <p>Your cart is empty</p>
        )}
      </div>
      <CheckoutButton onClick={checkoutPage} disabled={item.length === 0 ? true : false} />
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    item: state.addedToCart,
  };
};

export default connect(mapStateToProps)(withRouter(ShoppingCart));
