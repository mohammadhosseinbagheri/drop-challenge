import React from "react";
import { connect } from "react-redux";
import TabBarItem from "../tabbar-item/tabbar-item.component";
const FavoriteItem = (props) => {
  const { favoriteItem } = props;
  return <TabBarItem item={favoriteItem} />
};

const mapStateToProps = (state) => {
  return {
    favoriteItem: state.collections.selectedItem,
  };
};

export default connect(mapStateToProps)(FavoriteItem);
