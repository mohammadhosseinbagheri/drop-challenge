import React from "react";
import { Link } from "react-router-dom";

const FavoriteHeaderComponent = () => {
  return (
    <div
      style={{
        display: "flex",
        flexDirection: "column",
        justifyContent: "center",
        alignItems: "center",
        marginLeft: 10,
      }}
    >
      <Link style={{ color: "white", textDecoration: "none" }} to="/favorite">
        Favorite
      </Link>
    </div>
  );
};

export default FavoriteHeaderComponent;
