import React from "react";
import  './sort-item.styles.scss'
const SortItem = (props) => {
  const { name, value, handleChange, checked } = props;
  return (
    <div
    className={'sort-item-container'}
    >
      <p>{name}</p>
      <input
        name={name}
        onChange={(event) => handleChange(event)}
        type="radio"
        value={value}
        checked={checked}
      />
    </div>
  );
};

export default SortItem;
