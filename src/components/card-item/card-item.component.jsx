import React from "react";
import "./card-item.styles.scss";

import { connect } from "react-redux";
import { Star } from "@material-ui/icons";

import { selectItem } from "../../redux/collections/action/collection.acation";
import { showModalbox } from "../../redux/modalbox/action/modalbox.action";
import Modalbox from "../modalbox/modalbox.component";
const CardItem = (props) => {
  const { item, index, showModal, selectItem, favoriteItem } = props;

  const checkFavorites = (selected) => {
    const isSelected = favoriteItem.find((item) => item.id === selected.id);
    return isSelected;
  };
  return (
    <div className="card">
      <div className={"content"}>
        <img className="img" src={item.image_url} />
        <h5 style={{ textAlign: "center" }}>{item.name}</h5>
        <p>{item.tagline}</p>
        <button
          className="star-button"
          onClick={async () => {
            await selectItem(item);
            if (favoriteItem.length === 0) {
              localStorage.setItem(
                "favoriteExpire",
                JSON.stringify(new Date().getTime() + 30 * 24 * 60 * 60 * 1000)
              );
            }
          }}
        >
          <Star style={{ color: checkFavorites(item) ? "#f3d334" : "grey" }} />
        </button>
        <div className={"modal-botton"} onClick={() => showModal(item)}>
          <p>show modal</p>
        </div>
      </div>
    </div>
  );
};

const mapDispatchToProps = (dispatch) => {
  return {
    selectItem: (item) => {
      dispatch(selectItem(item));
    },
    showModal: (item) => dispatch(showModalbox(item)),
  };
};
const mapStateToProps = (state) => {
  return {
    favoriteItem: state.collections.selectedItem,
  };
};
export default connect(mapStateToProps, mapDispatchToProps, null, {
  forwardRef: true,
})(CardItem);
