import React from "react";

const CheckoutButton = (props) => {
  const {  disabled, onClick } = props;

  return (
    <button
      disabled={disabled}
      className="checkout-button"
      onClick={() => onClick()}
    >
      Check out
    </button>
  );
};

export default CheckoutButton;
