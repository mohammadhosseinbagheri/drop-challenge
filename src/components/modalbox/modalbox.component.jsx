import React from "react";
import { connect } from "react-redux";
import { addToCartAction } from "../../redux/add-to-cart/action/add-to-cart.action";
import { showModalbox } from "../../redux/modalbox/action/modalbox.action";

import "./modalbox.styles.scss";

const Modalbox = (props) => {
  const { modalItem, modalShow, addToCart, addedItem } = props;
  return (
    <div
      className={"modal-container"}
      onClick={() => {
        modalShow();
      }}
    >
      <div
        onClick={(event) => {
          event.stopPropagation();
        }}
        className={"modal-content"}
      >
        <img className={"item-img"} src={modalItem.image_url} />
        <h4 className={"item-name"}>{modalItem.name}</h4>
        <p className={"item-description"}>{modalItem.description}</p>
        <div className={"price-container"}>
          <p> abv : {modalItem.abv}</p>
          <p> price : {modalItem.srm}</p>
        </div>
        <div className={"item-tagline"}>{modalItem.tagline}</div>
        <button
          onClick={() => {
            addToCart(modalItem);
            if (addedItem.length === 0) {
              localStorage.setItem(
                "expireTime",
                JSON.stringify(new Date().getTime() + 7 * 24 * 60 * 60 * 1000)
              );
              return;
            }
          }}
          className={"button"}
        >
          <h5>Add to cart</h5>
        </button>
      </div>
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    isShow: state.showModal.isShow,
    modalItem: state.showModal.modalItem,
    addedItem: state.addedToCart,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    modalShow: () => {
      dispatch(showModalbox(""));
    },
    addToCart: (item) => dispatch(addToCartAction(item)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Modalbox);
