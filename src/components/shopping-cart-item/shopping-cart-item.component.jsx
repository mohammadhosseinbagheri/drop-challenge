import React from "react";

const ShoppingCartItem = (props) => {
  const { element } = props;

  return (
    <div className={"item-container"}>
      <div className={"item-content"}>
        <img src={element.image_url} alt="image" className={"item-img"} />
        <h5>{element.name}</h5>
      </div>

      <div className={"item-tagline"}>{element.tagline}</div>
    </div>
  );
};

export default ShoppingCartItem;
